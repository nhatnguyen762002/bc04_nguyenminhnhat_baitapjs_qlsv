function layThongTinTuForm() {
  const maSvValue = document.querySelector("#txtMaSV").value;
  const tenSvValue = document.querySelector("#txtTenSV").value;
  const emailValue = document.querySelector("#txtEmail").value;
  const matKhauValue = document.querySelector("#txtPass").value;
  const diemToanValue = document.querySelector("#txtDiemToan").value;
  const diemLyValue = document.querySelector("#txtDiemLy").value;
  const diemHoaValue = document.querySelector("#txtDiemHoa").value;

  return new SinhVien(
    maSvValue,
    tenSvValue,
    emailValue,
    matKhauValue,
    diemToanValue,
    diemLyValue,
    diemHoaValue
  );
}

function kiemTraThongTin(sv) {
  var isValid =
    validator.kiemTraRong(sv.ma, "spanMaSV", "Mã sinh viên") &&
    validator.kiemTraNumber(
      sv.ma,
      "spanMaSV",
      "Mã sinh viên chỉ được nhập số!"
    ) &&
    validator.kiemTraDoDai(
      sv.ma,
      "spanMaSV",
      "Mã sinh viên phải gồm 4 ký tự!",
      4,
      4
    ) &&
    validator.kiemTraMaTonTai(
      sv.ma,
      "spanMaSV",
      "Mã sinh viên đã tồn tại. Vui lòng nhập mã khác!",
      dsSv
    );

  isValid =
    isValid & validator.kiemTraRong(sv.ten, "spanTenSV", "Tên sinh viên");

  isValid =
    isValid &
    (validator.kiemTraRong(sv.email, "spanEmailSV", "Email") &&
      validator.kiemTraEmail(sv.email, "spanEmailSV", "Email không hợp lệ!"));

  isValid =
    isValid &
    (validator.kiemTraRong(sv.mk, "spanMatKhau", "Mật khẩu") &&
      validator.kiemTraMatKhauManh(
        sv.mk,
        "spanMatKhau",
        "Mật khẩu tối thiểu 8 ký tự. Chứa chữ hoa, chững thường, số và ký tự đặc biệt!"
      ));

  isValid =
    isValid &
    (validator.kiemTraRong(sv.toan, "spanToan", "Điểm toán") &&
      validator.kiemTraNumber(sv.toan, "spanToan", "Chỉ được nhập số!") &&
      validator.kiemTra0Den10(sv.toan, "spanToan", "Nhập điểm từ 0 - 10!"));

  isValid =
    isValid &
    (validator.kiemTraRong(sv.ly, "spanLy", "Điểm lý") &&
      validator.kiemTraNumber(sv.ly, "spanLy", "Chỉ được nhập số!") &&
      validator.kiemTra0Den10(sv.ly, "spanLy", "Nhập điểm từ 0 - 10!"));

  isValid =
    isValid &
    (validator.kiemTraRong(sv.hoa, "spanHoa", "Điểm hóa") &&
      validator.kiemTraNumber(sv.hoa, "spanHoa", "Chỉ được nhập số!") &&
      validator.kiemTra0Den10(sv.hoa, "spanHoa", "Nhập điểm từ 0 - 10!"));

  return isValid;
}

function kiemTraThongTinKhongMa(sv) {
  var isValid = validator.kiemTraRong(sv.ten, "spanTenSV", "Tên sinh viên");

  isValid =
    isValid &
    (validator.kiemTraRong(sv.email, "spanEmailSV", "Email") &&
      validator.kiemTraEmail(sv.email, "spanEmailSV", "Email không hợp lệ!"));

  isValid =
    isValid &
    (validator.kiemTraRong(sv.mk, "spanMatKhau", "Mật khẩu") &&
      validator.kiemTraMatKhauManh(
        sv.mk,
        "spanMatKhau",
        "Mật khẩu tối thiểu 8 ký tự. Chứa chữ hoa, chững thường, số và ký tự đặc biệt!"
      ));

  isValid =
    isValid &
    (validator.kiemTraRong(sv.toan, "spanToan", "Điểm toán") &&
      validator.kiemTraNumber(sv.toan, "spanToan", "Chỉ được nhập số!") &&
      validator.kiemTra0Den10(sv.toan, "spanToan", "Nhập điểm từ 0 - 10!"));

  isValid =
    isValid &
    (validator.kiemTraRong(sv.ly, "spanLy", "Điểm lý") &&
      validator.kiemTraNumber(sv.ly, "spanLy", "Chỉ được nhập số!") &&
      validator.kiemTra0Den10(sv.ly, "spanLy", "Nhập điểm từ 0 - 10!"));

  isValid =
    isValid &
    (validator.kiemTraRong(sv.hoa, "spanHoa", "Điểm hóa") &&
      validator.kiemTraNumber(sv.hoa, "spanHoa", "Chỉ được nhập số!") &&
      validator.kiemTra0Den10(sv.hoa, "spanHoa", "Nhập điểm từ 0 - 10!"));

  return isValid;
}

function resetForm() {
  document.querySelector("#txtMaSV").value = ``;
  document.querySelector("#txtTenSV").value = ``;
  document.querySelector("#txtEmail").value = ``;
  document.querySelector("#txtPass").value = ``;
  document.querySelector("#txtDiemToan").value = ``;
  document.querySelector("#txtDiemLy").value = ``;
  document.querySelector("#txtDiemHoa").value = ``;

  document.getElementById("spanMaSV").innerText = ``;
  document.getElementById("spanTenSV").innerText = ``;
  document.getElementById("spanEmailSV").innerText = ``;
  document.getElementById("spanMatKhau").innerText = ``;
  document.getElementById("spanToan").innerText = ``;
  document.getElementById("spanLy").innerText = ``;
  document.getElementById("spanHoa").innerText = ``;

  document.querySelector("#txtMaSV").readOnly = false;
  document.getElementById("btn-update").disabled = true;
  document.getElementById("btn-them-sv").disabled = false;
}

function resetSearch() {
  document.querySelector("#txtSearch").value = ``;
}

function resetDs() {
  document.querySelector("#tbodySinhVien").innerHTML = ``;
}

function renderDsSv(svArr) {
  var tbodyEl = document.querySelector("#tbodySinhVien");
  tbodyEl.innerHTML = ``;

  svArr.forEach(function (sv) {
    tbodyEl.innerHTML += `<tr>
    <td>${sv.ma}</td>
    <td>${sv.ten}</td>
    <td>${sv.email}</td>
    <td>${sv.tinhDTB()}</td>
    <td>
    <button onclick="xoaSinhVien('${
      sv.ma
    }')" class="btn btn-danger">Xóa</button>
    <button onclick="suaSinhVien('${
      sv.ma
    }')" class="btn btn-warning">Sửa</button>
    </td>
    </tr>`;
  });
}

function timKiemViTri(id, dssv) {
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];

    if (sv.ma == id) {
      return index;
    }
  }

  return -1;
}

function showThongTinLenForm(sv) {
  document.querySelector("#txtMaSV").value = sv.ma;
  document.querySelector("#txtTenSV").value = sv.ten;
  document.querySelector("#txtEmail").value = sv.email;
  document.querySelector("#txtPass").value = sv.mk;
  document.querySelector("#txtDiemToan").value = sv.toan;
  document.querySelector("#txtDiemLy").value = sv.ly;
  document.querySelector("#txtDiemHoa").value = sv.hoa;

  document.querySelector("#txtMaSV").readOnly = true;
}

function capNhatThongTinTuForm() {
  const maSvValue = document.querySelector("#txtMaSV").value;
  const tenSvValue = document.querySelector("#txtTenSV").value;
  const emailValue = document.querySelector("#txtEmail").value;
  const matKhauValue = document.querySelector("#txtPass").value;
  const diemToanValue = document.querySelector("#txtDiemToan").value;
  const diemLyValue = document.querySelector("#txtDiemLy").value;
  const diemHoaValue = document.querySelector("#txtDiemHoa").value;

  var newSv = new SinhVien(
    maSvValue,
    tenSvValue,
    emailValue,
    matKhauValue,
    diemToanValue,
    diemLyValue,
    diemHoaValue
  );

  var isValid = kiemTraThongTinKhongMa(newSv);
  if (isValid) {
    xoaSinhVien(maSvValue);
    dsSv.push(newSv);
    renderDsSv(dsSv);
    resetForm();
  }
}

function layThongTinSearch() {
  const searchValue = document.querySelector("#txtSearch").value;
  return searchValue;
}
