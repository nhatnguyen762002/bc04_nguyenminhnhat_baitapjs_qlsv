const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";
var dsSv = [];

document.getElementById("btn-update").disabled = true;

// Lấy thông tin từ localStorage
var dsSvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
if (dsSvJson != null) {
  dsSv = JSON.parse(dsSvJson);

  // Array khi convert thành json sẽ mất function, ta sẽ map lại
  for (var i = 0; i < dsSv.length; i++) {
    var sv = dsSv[i];

    dsSv[i] = new SinhVien(
      sv.ma,
      sv.ten,
      sv.email,
      sv.mk,
      sv.toan,
      sv.ly,
      sv.hoa
    );
  }
  renderDsSv(dsSv);
}

document.getElementById("btn-them-sv").addEventListener("click", function () {
  resetDs();
  resetSearch();
  var newSv = layThongTinTuForm();
  var isValid = kiemTraThongTin(newSv);
  if (isValid) {
    dsSv.push(newSv);

    // Tạo JSON
    var dsSvJson = JSON.stringify(dsSv);
    // Lưu JSON vào localStorage
    localStorage.setItem(DSSV_LOCALSTORAGE, dsSvJson);

    renderDsSv(dsSv);
    resetForm();
  }
});

function xoaSinhVien(id) {
  var index = timKiemViTri(id, dsSv);

  if (index != -1) {
    dsSv.splice(index, 1);
    renderDsSv(dsSv);
  }
}

function suaSinhVien(id) {
  resetForm();
  resetSearch();
  var index = timKiemViTri(id, dsSv);
  if (index != -1) {
    var sv = dsSv[index];
    showThongTinLenForm(sv);

    document.getElementById("btn-update").disabled = false;
    document.getElementById("btn-them-sv").disabled = true;
  }
}

document.getElementById("btn-update").addEventListener("click", function () {
  capNhatThongTinTuForm(dsSv);
  // Tạo JSON
  var dsSvJson = JSON.stringify(dsSv);
  // Lưu JSON vào localStorage
  localStorage.setItem(DSSV_LOCALSTORAGE, dsSvJson);
});

document.getElementById("btn-reset").addEventListener("click", function () {
  localStorage.removeItem(DSSV_LOCALSTORAGE);
  dsSv = [];
  renderDsSv(dsSv);
  resetForm();
  resetSearch();
});

document.getElementById("btnSearch").addEventListener("click", function () {
  resetForm();
  var searchValue = layThongTinSearch().toLowerCase();
  var dsSvFilter = dsSv.filter(function (sv) {
    var tenLowerCase = sv.ten.toLowerCase();
    return tenLowerCase.includes(searchValue);
  });

  if (dsSvFilter.length == 0) {
    document.querySelector("#tbodySinhVien").innerHTML = `<tr>
    <td class="text-danger">Sinh viên không tồn tại!</td>
    </tr>`;
  } else {
    renderDsSv(dsSvFilter);
  }
});
